﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_DVD_Library.Data.Factories;
using MVC_DVD_Library.Data.Interfaces;
using MVC_DVD_Library.Models;

namespace MVC_DVD_Library.BLL
{
    public class MovieManager
    {
        private IMovieRepository _repo;

        public MovieManager()
        {
            _repo = MovieRepositoryFactory.GetMovieRepository();
        }

        public IEnumerable<Movie> List()
        {
            var result = _repo.List();
            return result;
        }

        public Movie Get(int id)
        {
            var result = _repo.Get(id);
            return result;
        }

        public void Add(Movie movie)
        {
            _repo.Add(movie);
        }

        public void Edit(Movie movie)
        {
            _repo.Edit(movie);
        }

        public void Delete(int id)
        {
            _repo.Delete(id);
        }
    }
}
