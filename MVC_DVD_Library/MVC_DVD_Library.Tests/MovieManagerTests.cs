﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_DVD_Library.BLL;
using MVC_DVD_Library.Models;
using NUnit.Framework;

namespace MVC_DVD_Library.Tests
{
    [TestFixture]
    public class MovieManagerTests
    {
        [Test]
        public void CanListMovieRecords()
        {
            var mgr = new MovieManager();
            var result = mgr.List();

            Assert.AreEqual(3, result.ToList().Count);
        }

        [TestCase(1, "Joel Coen", "Keith Manecke")]
        [TestCase(2, "Shane Carruth", "Joe Jones")]
        public void CanGetSpecificMovieRecord(int movieId, string director, string borrower)
        {
            var mgr = new MovieManager();
            var movie = mgr.Get(movieId);
            Assert.AreEqual(director, movie.DirectorName);
            Assert.AreEqual(borrower, movie.BorrowerName);
        }

        [TestCase(4, "Big Movie", "Bob Bobberson", 2)]
        [TestCase(5, "Cinema Masterpiece", "Monkey Man", 1)]
        public void CanAddMovieRecord(int id, string title, string director, int userRating)
        {
            var mgr = new MovieManager();
            var movie = new Movie();
            movie.Title = title;
            movie.DirectorName = director;
            movie.UserRating = userRating;
            mgr.Add(movie);
            var newMovie = mgr.Get(id);
            Assert.AreEqual(newMovie.Title, title);
            Assert.AreEqual(newMovie.DirectorName, director);
            Assert.AreEqual(newMovie.UserRating, userRating);
        }

        [TestCase(2, "Big Movie", "Bob Bobberson", 2)]
        [TestCase(3, "Cinema Masterpiece", "Monkey Man", 1)]
        public void CanEditApplication(int id, string title, string director, int userRating)
        {
            var mgr = new MovieManager();
            var movie = mgr.Get(id);
            movie.Title = title;
            movie.DirectorName = director;
            movie.UserRating = userRating;
            mgr.Edit(movie);
            var editedMovie = mgr.Get(id);
            Assert.AreEqual(editedMovie.Title, title);
            Assert.AreEqual(editedMovie.DirectorName, director);
            Assert.AreEqual(editedMovie.UserRating, userRating);
        }

        [Test]
        public void CanDeleteApplication()
        {
            var mgr = new MovieManager();
            mgr.Delete(2);
            var movies = mgr.List();
            Assert.AreEqual(movies.ToList().Count, 2);
        }
    }
}
