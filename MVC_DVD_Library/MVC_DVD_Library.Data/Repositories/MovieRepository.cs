﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_DVD_Library.Data.Interfaces;
using MVC_DVD_Library.Models;

namespace MVC_DVD_Library.Data.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        public IEnumerable<Movie> List()
        {
            throw new NotImplementedException();
        }

        public Movie Get(int movieId)
        {
            throw new NotImplementedException();
        }

        public void Add(Movie movie)
        {
            throw new NotImplementedException();
        }

        public void Edit(Movie movie)
        {
            throw new NotImplementedException();
        }

        public void Delete(int movieId)
        {
            throw new NotImplementedException();
        }
    }
}
