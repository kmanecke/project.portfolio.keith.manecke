﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MVC_DVD_Library.BLL;
using MVC_DVD_Library.Models;

namespace MVC_DVD_Library.UI.Controllers
{
    public class MoviesController : ApiController
    {
        public IEnumerable<Movie> Get()
        {
            var mgr = new MovieManager();
            return mgr.List();
        }

        [Route("api/movies/search/{title}")]
        [HttpGet]
        public Movie GetByTitle(string title)
        {
            //Attribute routing
            //Enable Attribute routing in WebApiConfig: actually was enabled by default

            var mgr = new MovieManager();
            var movies = mgr.List();
            var movie = movies.FirstOrDefault(d => d.Title.ToUpper() == title.ToUpper());
            if (movie == null)
            {
                var result = new Movie();
                result.Title = "Title Not Found";
                result.UserRating = 99999;
                return result;
            }
            return mgr.Get(movie.MovieId);
        }

        [HttpGet]
        public Movie GetById(int id)
        {
            var mgr = new MovieManager();
            return mgr.Get(id);
        }

        [Route("api/movies/add/")]
        [HttpPost]
        public HttpResponseMessage PostAddMovie(Movie movie)
        {
            var mgr = new MovieManager();
            mgr.Add(movie);
            var response = Request.CreateResponse(HttpStatusCode.Created);
            return response;
        }

        [Route("api/movies/loan/{id}/{name}")]
        [HttpPost]
        public HttpResponseMessage PostLoanDetails(int id, string name)
        {
            var mgr = new MovieManager();
            var movieRecord = mgr.Get(id);
            var date = DateTime.Now;
            movieRecord.BorrowedDate = date;
            movieRecord.BorrowerName = name;
            mgr.Edit(movieRecord);
            var response = Request.CreateResponse(HttpStatusCode.Created);
            return response;
        }

        [Route("api/movies/loan/{id}")]
        [HttpPost]
        public HttpResponseMessage PostReturnDetails(int id)
        {
            var mgr = new MovieManager();
            var movieRecord = mgr.Get(id);
            var date = DateTime.Now;
            movieRecord.ReturnedDate = null;
            movieRecord.BorrowedDate = null;
            movieRecord.BorrowerName = null;
            mgr.Edit(movieRecord);
            var response = Request.CreateResponse(HttpStatusCode.Created);
            return response;
        }

        [Route("api/movies/delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteReturnDetails(int id)
        {
            var mgr = new MovieManager();
            mgr.Delete(id);
            var response = Request.CreateResponse(HttpStatusCode.Created);
            return response;
        }
    }
}
