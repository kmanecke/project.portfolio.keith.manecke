﻿var uri = '/api/movies/search/';

$(document)
    .ready(function () {
        $('#btnSearchByTitle')
            .on('click',
                function() {
                    findMovie();
                });
    });

function findMovie() {
    var title = $('#title').val();

    $.getJSON(uri + title)
        .done(function(data) {
            if (!CheckForNullMovieObject(data)) {
                $('#movieTable tbody tr, #movieTable thead tr, #errorMessage thead tr').remove();
                $(createHeadingsForSearchResult()).appendTo($('#movieTable thead'));
                $(createRowForSearchResult(data)).appendTo($('#movieTable tbody'));
            } else {
                $('#movieTable tbody tr, #movieTable thead tr, #errorMessage thead tr').remove();
                $('<tr><th>Title Not Found</th></tr>').appendTo('#errorMessage thead');
            }
        })
        .fail(function (jqXhr, status, err) {
            $('#errorMessage thead tr').text('Error: ' + err);
        });
}

function CheckForNullMovieObject(data) {
    if (data.Title === 'Title Not Found' && data.UserRating === 99999) {
        return true;
    } else {
        return false;
    };

}

function createHeadingsForSearchResult() {
    return '<tr><th>ID</th><th>Title</th><th>Release Date</th>' +
        '<th>Rating</th><th>Borrower Name</th></tr>';
}

function createRowForSearchResult(data) {
    return '<tr><td>' +
        data.MovieId +
        '</td><td>' +
        data.Title +
        '</td><td>' +
        data.ReleaseDate.substr(0, 4) +
        '</td><td>' +
        data.MpaaRating +
        '</td><td>' +
        checkBorrowerName(data.BorrowerName) +
        '</td></tr>';
}
