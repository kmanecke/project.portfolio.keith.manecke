using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using models;
using models.Interfaces.Repositories;

namespace data.Repositories 
{
    public class BookRepository : IBookRepository 
    {
        private readonly ReadingLogContext context;
        
        public BookRepository(ReadingLogContext context) 
        {
            this.context = context;
        }

        public async Task<IList<Book>> GetBooks()
        {
            return await context.Books
                .OrderBy(b => b.DateFinished)
                .Include(b => b.Author)
                .ToListAsync();
        }

        public async Task<Book> GetBook(int bookID)
        {
            var result = await context.Books
                .Where(b => b.ID == bookID)
                .Include(b => b.Author)
                .FirstOrDefaultAsync();

            if (result == null)
                throw new EntityNotFoundException("Book", bookID);

            return result;
        }

        public async Task AddBook(Book book)
        {
            await context.Books.AddAsync(book);
            await context.SaveChangesAsync();
        }

        public async Task EditBook(Book book) 
        {
            var bookToEdit = await context.Books.FindAsync(book.ID);
            bookToEdit.Title = book.Title;
            bookToEdit.DateFinished = book.DateFinished;
            bookToEdit.Comment = book.Comment;
            bookToEdit.AuthorID = book.AuthorID;
            await context.SaveChangesAsync();
        }

        public async Task DeleteBook(int bookID)
        {
            var bookToDelete = await context.Books.FindAsync(bookID);

            if (bookToDelete == null)
                throw new EntityNotFoundException("Book", bookID);

            context.Books.Remove(bookToDelete);
            await context.SaveChangesAsync();
        }
    }
}