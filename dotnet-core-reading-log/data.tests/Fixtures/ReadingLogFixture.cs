using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Xunit;
using models;
using data.tests.TestData;

namespace data.tests.Fixtures 
{
    public class ReadingLogFixture : IDisposable
    {
        public IReadingLogTestData testData { get; private set; }
        public ReadingLogContext context { get; private set; }

        public ReadingLogFixture() 
        {
            CreateTestData();
        }

        private void CreateTestData() 
        {
            var options = new DbContextOptionsBuilder<ReadingLogContext>()
                .UseInMemoryDatabase(databaseName: "test_db_context")
                .Options;
            
            context = new ReadingLogContext(options);
            testData = new ReadingLogTestData();

            context.Books.AddRange(testData.Books);
            context.Authors.AddRange(testData.Authors);
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}