using System;
using System.Collections.Generic;
using System.Linq;
using models;

namespace data.tests.TestData 
{
    public interface IReadingLogTestData
    {
        IList<Book> Books { get; set;}
        IList<Author> Authors { get; set; }
    }
}