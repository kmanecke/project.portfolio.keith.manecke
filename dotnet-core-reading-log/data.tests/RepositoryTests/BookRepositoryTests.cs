using System;
using Xunit;
using System.Linq;
using data.tests.Fixtures;
using data.Repositories;
using models;
using models.Interfaces.Repositories;

namespace data.tests.RepositoryTests 
{
    public class BookRepositoryTests : IClassFixture<ReadingLogFixture>
    {
        private ReadingLogFixture fixture;
        private IBookRepository repository;

        public BookRepositoryTests(ReadingLogFixture fixture) 
        {
            this.fixture = fixture;
            repository = new BookRepository(fixture.context);
        }

        [Fact]
        public async void GetBooksReturnsAllBookRecords() 
        {
            var books = await repository.GetBooks();
            Assert.Equal(2, books.Count);
        }

        [Fact]
        public async void GetBookReturnsCorrectBookRecord() 
        {
            var book = await repository.GetBook(1);
            Assert.Equal("Test Book 1", book.Title);
            Assert.Equal(10, book.AuthorID);
        }

        [Fact]
        public async void AddBookSucceeds() 
        {
            var book = new Book() 
            {
                ID = 100,
                Title = "Added Book 3",
                DateFinished = new DateTime(2010, 01, 01),
                Comment = "hello comments",
                AuthorID = 10
            };
            await repository.AddBook(book);

            var newBook = await repository.GetBook(100);
            Assert.Equal("hello comments", newBook.Comment);
            Assert.Equal("John Doe", newBook.Author.Name);
        }

        [Fact]
        public async void EditBookSucceeds() 
        {
            var book = await repository.GetBook(1);
            book.Title = "New Book 1 Title";
            book.Comment = "Book 1 updated successfully";
            book.AuthorID = 11;

            await repository.EditBook(book);

            var updatedBook = await repository.GetBook(1);
            Assert.Equal("New Book 1 Title", updatedBook.Title);
            Assert.Equal("Jane Doe", updatedBook.Author.Name);
        }

        [Fact]
        public async void DeleteBookSucceeds() 
        {
            await repository.DeleteBook(2);

            var books = await repository.GetBooks();
            Assert.Equal(1, books.Count);
        }
    }
}