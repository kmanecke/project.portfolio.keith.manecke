using System;

namespace models
{
    public class EntityNotFoundException: Exception 
    {
        public EntityNotFoundException(string name, object key) 
        : base($"{name} entity with id {key} was not found") 
        {
        }
    }
}
