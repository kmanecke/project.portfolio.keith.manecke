using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using models;

namespace models.Interfaces.Repositories 
{
    public interface IBookRepository
    {
        Task<IList<Book>> GetBooks();
        Task<Book> GetBook(int bookID);
        Task AddBook(Book book);
        Task EditBook(Book book);
        Task DeleteBook(int bookID);
    }
}