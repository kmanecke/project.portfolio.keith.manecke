using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace models 
{
    public class Author 
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public ICollection<Book> Books { get; set; }
    }

}