using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using data;
using models;

namespace api.Services 
{
    public interface IAuthorService
    {
        Task<IList<Author>> GetAuthors();
        Task<Author> GetAuthor(int authorID);
        Task AddAuthor(Author author);
        Task EditAuthor(Author author);
        Task DeleteAuthor(int authorID);   
    }
}