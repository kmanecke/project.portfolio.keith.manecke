using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using models;
using api.Services;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase 
    {
        private readonly IBookService bookService;

        public BookController(IBookService bookService) 
        {
            this.bookService = bookService;
        }

        // GET api/book
        [HttpGet]
        public IActionResult GetBooks()
        {
            return Ok(bookService.GetBooks().Result);
        }

        // GET api/book/5
        [HttpGet("{bookID}")]
        public IActionResult GetBook(int bookID)
        {
            return Ok(bookService.GetBook(bookID).Result);
        }

        //POST api/book
        [HttpPost]
        public void Post([FromBody] Book postData) 
        {
            bookService.AddBook(postData);
        }

        //POST api/book/edit
        [HttpPost("edit")]
        public void EditBook([FromBody] Book postData) 
        {
            bookService.EditBook(postData);
        }

        //DELETE api/book/delete/5
        [HttpDelete("delete/{bookID}")]
        public void DeleteBook(int bookID) 
        {
            bookService.DeleteBook(bookID);
        }
    }
}