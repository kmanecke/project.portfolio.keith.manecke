using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using data;
using models;
using api.Services;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase 
    {
        private readonly IAuthorService authorService;

        public AuthorController(IAuthorService authorService) 
        {
            this.authorService = authorService;
        }

        // GET api/author
        [HttpGet]
        public IActionResult GetAuthors()
        {
            return Ok(authorService.GetAuthors().Result);
        }

        // GET api/author/5
        [HttpGet("{authorID}")]
        public IActionResult GetAuthor(int authorID)
        {
            return Ok(authorService.GetAuthor(authorID).Result);
        }

        //POST api/author
        [HttpPost]
        public void Post([FromBody] Author postData) 
        {
            authorService.AddAuthor(postData);
        }

        //POST api/author/edit
        [HttpPost("edit")]
        public void EditAuthor([FromBody] Author postData) 
        {
            authorService.EditAuthor(postData);
        }

        //DELETE api/author/delete/5
        [HttpDelete("delete/{authorID}")]
        public void DeleteAuthor(int authorID) 
        {
            authorService.DeleteAuthor(authorID);
        }
    }
}