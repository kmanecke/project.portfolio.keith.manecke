﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SG_HR_Portal.BLL;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Tests
{
    [TestFixture]
    public class CategoryManagerTests
    {
        [Test]
        public void CanListAllCategories()
        {
            var mgr = new CategoryManager();
            var result = mgr.List();
            Assert.AreEqual(result.ToList().Count, 2);
        }

        [TestCase("Leave Time", 2)]
        [TestCase("Onboarding New Hires", 1)]
        public void CanGetSpecificCategories(string title, int catId)
        {
            var mgr = new CategoryManager();
            var category = mgr.Get(catId);
            Assert.AreEqual(category.CategoryTitle, title);
            Assert.AreEqual(category.CategoryId, catId);
        }

        [TestCase(3, "Food at Work")]
        [TestCase(4, "Termination")]
        public void CanAddCategory(int id, string title)
        {
            var mgr = new CategoryManager();
            var category = new Category();
            category.CategoryId = id;
            category.CategoryTitle = title;
            mgr.Add(category);
            var newCategory = mgr.Get(id);
            Assert.AreEqual(newCategory.CategoryId, id);
            Assert.AreEqual(newCategory.CategoryTitle, title);
        }

        [TestCase(1, "Making Paper Airplanes", "New description text")]
        [TestCase(2, "Dress Code", "new dress code text")]
        public void CanEditCategories(int id, string title, string description)
        {
            var mgr = new CategoryManager();
            var position = mgr.Get(id);
            position.CategoryTitle = title;
            position.CategoryDescription = description;
            mgr.Edit(position);
            var catEdited = mgr.Get(id);
            Assert.AreEqual(catEdited.CategoryTitle, title);
            Assert.AreEqual(catEdited.CategoryDescription, description);
        }

        [Test]
        public void CanDeleteCategory()
        {
            var mgr = new CategoryManager();
            mgr.Delete(1);
            var categories = mgr.List();
            Assert.AreEqual(categories.ToList().Count, 1);
        }
    }
}
