﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SG_HR_Portal.Data.Repositories;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Tests
{
    [TestFixture]
    public class MockApplicationRepositoryTests
    {
        [Test]
        public void CanListAllApplications()
        {
            var repo = new MockApplicationRepository();
            var apps = repo.List();

            Assert.AreEqual(3, apps.ToList().Count);

        }

        [TestCase(1, "Keith Manecke")]
        [TestCase(3, "Kristina Emick")]
        public void CanGetSpecificApplications(int applicationNumber, string expected)
        {
            var repo = new MockApplicationRepository();
            var app = repo.Get(applicationNumber);
            Assert.AreEqual(expected, app.ApplicantName);
        }

        [TestCase(4, "Bob Bobberson", "bob@email.com")]
        [TestCase(5, "Monkey Man", "monkey@email.com")]
        public void CanAddApplication(int id, string name, string email)
        {
            var repo = new MockApplicationRepository();
            var app = new Application();
            app.ApplicationId = id;
            app.ApplicantName = name;
            app.ApplicantEmail = email;
            //app.Position = position;
            repo.Add(app);
            var newApp = repo.Get(id);
            Assert.AreEqual(newApp.ApplicantName, name);
            Assert.AreEqual(newApp.ApplicantEmail, email);
            //Assert.AreEqual(newApp.Position, position);
        }

        [TestCase(2, "Bob Bobberson", "bob@email.com")]
        [TestCase(3, "Monkey Man", "monkey@email.com")]
        public void CanEditApplications(int id, string name, string email)
        {
            var repo = new MockApplicationRepository();
            var app = repo.Get(id);
            app.ApplicantName = name;
            app.ApplicantEmail = email;
            //app.Position = position;
            repo.Edit(app);
            var appEdited = repo.Get(id);
            Assert.AreEqual(appEdited.ApplicantName, name);
            Assert.AreEqual(appEdited.ApplicantEmail, email);
            //Assert.AreEqual(appEdited.Position, position);
            
        }

        [Test]
        public void CanDeleteApplication()
        {
            var repo = new MockApplicationRepository();
            repo.Delete(3);
            var apps = repo.List();
            Assert.AreEqual(apps.ToList().Count, 2);
        }
    }
}
