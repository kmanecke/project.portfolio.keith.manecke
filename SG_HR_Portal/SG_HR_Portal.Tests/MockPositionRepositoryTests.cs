﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SG_HR_Portal.Data.Repositories;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Tests
{
    [TestFixture]
    public class MockPositionRepositoryTests
    {
        [Test]
        public void CanListAllPositions()
        {
            var repo = new MockPositionRepository();
            var positions = repo.List();

            Assert.AreEqual(4, positions.ToList().Count);
        }

        [TestCase(1, 50000, "Junior Developer")]
        [TestCase(3, 80000, "Communications Director")]
        public void CanGetSpecificPositions(int positionId, decimal salary, string title)
        {
            var repo = new MockPositionRepository();
            var position = repo.Get(positionId);
            Assert.AreEqual(position.PostedSalary, salary);
            Assert.AreEqual(position.PositionTitle, title);
        }

        [TestCase(5, "Superstar", 40000, "Ms. Chairperson")]
        [TestCase(6, "Salesperson", 35000, "Mr. Executive")]
        public void CanAddPosition(int id, string title, decimal salary, string manager)
        {
            var repo = new MockPositionRepository();
            var position = new Position();
            position.PositionId = id;
            position.PositionTitle = title;
            position.PostedSalary = salary;
            position.HiringManager = manager;
            repo.Add(position);
            var newPosition = repo.Get(id);
            Assert.AreEqual(newPosition.PositionTitle, title);
            Assert.AreEqual(newPosition.PostedSalary, salary);
            Assert.AreEqual(newPosition.HiringManager, manager);
        }

        [TestCase(2, "Big Man on Campus", 30000, "Bob Boberson")]
        [TestCase(3, "Monkey Wrangler", 65000, "George Gorilla")]
        public void CanEditPositions(int id, string title, decimal salary, string manager)
        {
            var repo = new MockPositionRepository();
            var position = repo.Get(id);
            position.PositionTitle = title;
            position.PostedSalary = salary;
            position.HiringManager = manager;
            repo.Edit(position);
            var posEdited = repo.Get(id);
            Assert.AreEqual(posEdited.PositionTitle, title);
            Assert.AreEqual(posEdited.PostedSalary, salary);
            Assert.AreEqual(posEdited.HiringManager, manager);
        }

        [Test]
        public void CanDeleteApplication()
        {
            var repo = new MockPositionRepository();
            repo.Delete(2);
            var positions = repo.List();
            Assert.AreEqual(positions.ToList().Count, 3);
        }
    }
}
