﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SG_HR_Portal.BLL;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Tests
{
    [TestFixture]
    public class PositionManagerTests
    {
        [Test]
        public void CanListAllPositions()
        {
            var mgr = new PositionManager();
            var result = mgr.List();
            Assert.AreEqual(result.ToList().Count, 4);
        }

        [TestCase(2, "Senior Developer", "Big Boss Man")]
        [TestCase(4, "Custodian", "Big Boss Lady")]
        public void CanGetSpecificPositions(int id, string title, string manager)
        {
            var mgr = new PositionManager();
            var result = mgr.Get(id);
            Assert.AreEqual(result.PositionTitle, title);
            Assert.AreEqual(result.HiringManager, manager);
        }

        [TestCase(5, "Dog Walker", 36000, "Mr. Anderson")]
        [TestCase(6, "Bean Counter", 44000, "Agent Smith")]
        public void CanAddPosition(int id, string title, decimal salary, string manager)
        {
            var mgr = new PositionManager();
            var position = new Position();
            position.PositionTitle = title;
            position.PostedSalary = salary;
            position.HiringManager = manager;
            mgr.Add(position);
            var newPosition = mgr.Get(id);
            Assert.AreEqual(newPosition.PositionTitle, title);
            Assert.AreEqual(newPosition.PostedSalary, salary);
            Assert.AreEqual(newPosition.HiringManager, manager);
        }

        [TestCase(3, "Cat Wrangler", "Joe Jones")]
        [TestCase(4, "Wallet Inspector", "Roddy Piper")]
        public void CanEditPositions(int id, string title, string manager)
        {
            var mgr = new PositionManager();
            var position = mgr.Get(id);
            position.PositionTitle = title;
            position.HiringManager = manager;
            mgr.Edit(position);
            var posEdited = mgr.Get(id);
            Assert.AreEqual(posEdited.PositionTitle, title);
            Assert.AreEqual(posEdited.HiringManager, manager);
        }

        [Test]
        public void CanDeleteApplication()
        {
            var mgr = new PositionManager();
            mgr.Delete(3);
            var positions = mgr.List();
            Assert.AreEqual(positions.ToList().Count, 3);
        }
    }
}
