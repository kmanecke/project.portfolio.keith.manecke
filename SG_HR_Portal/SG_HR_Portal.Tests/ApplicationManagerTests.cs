﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SG_HR_Portal.BLL;
using SG_HR_Portal.Data.Repositories;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Tests
{
    [TestFixture]
    public class ApplicationManagerTests
    {
        [Test]
        public void CanListAllApplications()
        {
            var mgr = new ApplicationManager();
            var result = mgr.List();
            Assert.AreEqual(result.ToList().Count, 3);
        }

        [TestCase(2, "Dave Balzer", "Senior Developer")]
        [TestCase(3, "Kristina Emick", "Communications Director")]
        public void CanGetSpecificApplications(int applicationNumber, string name, string position)
        {
            var mgr = new ApplicationManager();
            var result = mgr.Get(applicationNumber);
            Assert.AreEqual(result.ApplicantName, name);
            Assert.AreEqual(result.Position.PositionTitle, position);
        }

        [TestCase(4, "Bob Bobberson", "bob@email.com")]
        [TestCase(5, "Monkey Man", "monkey@email.com")]
        public void CanAddApplication(int id, string name, string email)
        {
            var mgr = new ApplicationManager();
            var app = new Application();
            //app.ApplicationId = id;
            app.ApplicantName = name;
            app.ApplicantEmail = email;
            //app.Position = position;
            mgr.Add(app);
            var newApp = mgr.Get(id);
            Assert.AreEqual(newApp.ApplicantName, name);
            Assert.AreEqual(newApp.ApplicantEmail, email);
            //Assert.AreEqual(newApp.Position, position);
        }

        [TestCase(2, "Bob Bobberson", "bob@email.com")]
        [TestCase(3, "Monkey Man", "monkey@email.com")]
        public void CanEditApplications(int id, string name, string email)
        {
            var mgr = new ApplicationManager();
            var app = mgr.Get(id);
            app.ApplicantName = name;
            app.ApplicantEmail = email;
            //app.Position = position;
            mgr.Edit(app);
            var appEdited = mgr.Get(id);
            Assert.AreEqual(appEdited.ApplicantName, name);
            Assert.AreEqual(appEdited.ApplicantEmail, email);
            //Assert.AreEqual(appEdited.Position, position);

        }

        [Test]
        public void CanDeleteApplication()
        {
            var mgr = new ApplicationManager();
            mgr.Delete(1);
            var apps = mgr.List();
            Assert.AreEqual(apps.ToList().Count, 2);
        }
    }
}
