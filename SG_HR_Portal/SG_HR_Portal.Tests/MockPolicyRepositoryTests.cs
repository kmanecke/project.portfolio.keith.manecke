﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SG_HR_Portal.Data.Repositories;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Tests
{
    [TestFixture]
    public class MockPolicyRepositoryTests
    {
        [Test]
        public void CanListAllPolicies()
        {
            var repo = new MockPolicyRepository();
            var policies = repo.List();

            Assert.AreEqual(3, policies.ToList().Count);
        }

        [TestCase(1, "Mentoring Program", 1)]
        [TestCase(3, "Vacation Leave", 2)]
        public void CanGetSpecificPolicy(int id, string title, int catId)
        {
            var repo = new MockPolicyRepository();
            var policy = repo.Get(id);
            Assert.AreEqual(title, policy.PolicyTitle);
            Assert.AreEqual(catId, policy.PolicyCategory.CategoryId);
        }

        [TestCase(4, "Proper Footwear", "policy description")]
        [TestCase(5, "Personal Time", "personal time text")]
        public void CanAddPolicy(int id, string title, string content)
        {
            var repo = new MockPolicyRepository();
            var policy = new Policy();
            policy.PolicyTitle = title;
            policy.PolicyContent = content;
            repo.Add(policy);
            var newPolicy = repo.Get(id);
            Assert.AreEqual(newPolicy.PolicyTitle, title);
            Assert.AreEqual(newPolicy.PolicyContent, content);
        }

        [TestCase(2, "Proper Footwear", "policy description")]
        [TestCase(3, "Personal Time", "personal time text")]
        public void CanEditPolicy(int id, string title, string content)
        {
            var repo = new MockPolicyRepository();
            var policy = repo.Get(id);
            policy.PolicyTitle = title;
            policy.PolicyContent = content;
            repo.Edit(policy);
            var editedPolicy = repo.Get(id);
            Assert.AreEqual(editedPolicy.PolicyTitle, title);
            Assert.AreEqual(editedPolicy.PolicyContent, content);
        }

        [Test]
        public void CanDeletePolicy()
        {
            var repo = new MockPolicyRepository();
            repo.Delete(2);
            var policies = repo.List();
            Assert.AreEqual(policies.ToList().Count, 2);
        }
    }
}
