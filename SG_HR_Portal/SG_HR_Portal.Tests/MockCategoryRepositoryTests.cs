﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SG_HR_Portal.Data.Repositories;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Tests
{
    [TestFixture]
    public class MockCategoryRepositoryTests
    {

        [Test]
        public void CanListAllCategories()
        {
            var repo = new MockCategoryRepository();
            var positions = repo.List();

            Assert.AreEqual(2, positions.ToList().Count);
        }

        [TestCase("Leave Time", 2)]
        [TestCase("Onboarding New Hires", 1)]
        public void CanGetSpecificCategories(string title, int catId)
        {
            var repo = new MockCategoryRepository();
            var category = repo.Get(catId);
            Assert.AreEqual(category.CategoryTitle, title);
            Assert.AreEqual(category.CategoryId, catId);
        }

        [TestCase(3, "Food at Work")]
        [TestCase(4, "Termination")]
        public void CanAddCategory(int id, string title)
        {
            var repo = new MockCategoryRepository();
            var category = new Category();
            category.CategoryId = id;
            category.CategoryTitle = title;
            repo.Add(category);
            var newCategory = repo.Get(id);
            Assert.AreEqual(newCategory.CategoryId, id);
            Assert.AreEqual(newCategory.CategoryTitle, title);
        }

        [TestCase(1, "Making Paper Airplanes", "New description text")]
        [TestCase(2, "Dress Code", "new dress code text")]
        public void CanEditCategories(int id, string title, string description)
        {
            var repo = new MockCategoryRepository();
            var position = repo.Get(id);
            position.CategoryTitle = title;
            position.CategoryDescription = description;
            repo.Edit(position);
            var catEdited = repo.Get(id);
            Assert.AreEqual(catEdited.CategoryTitle, title);
            Assert.AreEqual(catEdited.CategoryDescription, description);
        }

        [Test]
        public void CanDeleteCategory()
        {
            var repo = new MockCategoryRepository();
            repo.Delete(1);
            var categories = repo.List();
            Assert.AreEqual(categories.ToList().Count, 1);
        }
    }
}
