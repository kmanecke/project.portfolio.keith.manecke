﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SG_HR_Portal.BLL;
using SG_HR_Portal.Models.DataModels;
using SG_HR_Portal.Models.ViewModels;

namespace SG_HR_Portal.UI.Controllers
{
    public class ApplicationController : Controller
    {
        // GET: Application
        [HttpGet]
        public ActionResult List()
        {
            var appManager = new ApplicationManager();
            var model = appManager.List();
            return View(model.ToList());
        }

        [HttpGet]
        public ActionResult Add()
        {
            var posManager = new PositionManager();
            var viewModel = new ApplicationVM();
            viewModel.SetPositionItems(posManager.List());
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Add(ApplicationVM applicationVM)
        {
            var posManager = new PositionManager();
            applicationVM.Application.Position = posManager.Get(applicationVM.Application.Position.PositionId);

            var appManager = new ApplicationManager();
            appManager.Add(applicationVM.Application);

            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var viewModel = new ApplicationVM();

            var appManager = new ApplicationManager();
            var application = appManager.Get(id);
            viewModel.Application = application;

            var posManager = new PositionManager();
            viewModel.SetPositionItems(posManager.List());

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(ApplicationVM applicationVM)
        {

            var posManager = new PositionManager();
            applicationVM.Application.Position = posManager.Get(applicationVM.Application.Position.PositionId);

            var appManager = new ApplicationManager();
            appManager.Edit(applicationVM.Application);

            return RedirectToAction("List");

        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var appManager = new ApplicationManager();
            var app = appManager.Get(id);
            return View(app);
        }

        [HttpPost]
        public ActionResult Delete(Application application)
        {
            var appManager = new ApplicationManager();
            appManager.Delete(application.ApplicationId);
            return RedirectToAction("List");
        }
    }
}