﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SG_HR_Portal.BLL;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.UI.Controllers
{
    public class PositionController : Controller
    {
        // GET: Position
        [HttpGet]
        public ActionResult List()
        {
            var posManager = new PositionManager();
            var model = posManager.List();
            return View(model.ToList());
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View(new Position());
        }

        [HttpPost]
        public ActionResult Add(Position position)
        {
            var posManager = new PositionManager();
            posManager.Add(position);
            return RedirectToAction("List");

        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var posManager = new PositionManager();
            var position = posManager.Get(id);
            return View(position);
        }

        [HttpPost]
        public ActionResult Edit(Position position)
        {
            var posManager = new PositionManager();
            posManager.Edit(position);
            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var posManager = new PositionManager();
            var pos = posManager.Get(id);
            return View(pos);
        }

        [HttpPost]
        public ActionResult Delete(Position position)
        {
            var posManager = new PositionManager();
            posManager.Delete(position.PositionId);
            return RedirectToAction("List");
        }
    }
}