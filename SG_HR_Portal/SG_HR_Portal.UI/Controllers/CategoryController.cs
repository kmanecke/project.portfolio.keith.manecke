﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SG_HR_Portal.BLL;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.UI.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        [HttpGet]
        public ActionResult Manage()
        {
            var catManager = new CategoryManager();
            var model = catManager.List();
            return View(model.ToList());
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View(new Category());
        }

        [HttpPost]
        public ActionResult Add(Category category)
        {
            if (ModelState.IsValid)
            {
                var catManager = new CategoryManager();
                catManager.Add(category);
                return RedirectToAction("Manage");
            }
            else
            {
                return View(category);
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var catManager = new CategoryManager();
            var category = catManager.Get(id);
            return View(category);
        }

        [HttpPost]
        public ActionResult Edit(Category category)
        {
            var catManager = new CategoryManager();
            catManager.Edit(category);
            return RedirectToAction("Manage");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var catManager = new CategoryManager();
            var cat = catManager.Get(id);
            return View(cat);
        }

        [HttpPost]
        public ActionResult Delete(Category category)
        {
            var catManager = new CategoryManager();
            catManager.Delete(category.CategoryId);
            return RedirectToAction("Manage");
        }
    }
}