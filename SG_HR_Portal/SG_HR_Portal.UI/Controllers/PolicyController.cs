﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SG_HR_Portal.BLL;
using SG_HR_Portal.Models.DataModels;
using SG_HR_Portal.Models.ViewModels;

namespace SG_HR_Portal.UI.Controllers
{
    public class PolicyController : Controller
    {
        // GET: Policy
        [HttpGet]
        public ActionResult List()
        {
            var polManager = new PolicyManager();
            var model = polManager.List();
            
            return View(model);
        }

        [HttpGet]
        public ActionResult ListAll()
        {
            var polManager = new PolicyManager();
            var model = polManager.List();
            return View(model);
        }

        [HttpGet]
        public ActionResult Manage()
        {
            var polManager = new PolicyManager();
            var model = polManager.List();
            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var catManager = new CategoryManager();
            var viewModel = new PolicyVM();
            viewModel.SetCategoryItems(catManager.List());
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Add(PolicyVM policyVM)
        {
            var catManager = new CategoryManager();
            policyVM.Policy.PolicyCategory = catManager.Get(policyVM.Policy.PolicyCategory.CategoryId);

            var polManager = new PolicyManager();
            polManager.Add(policyVM.Policy);

            return RedirectToAction("Manage");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var viewModel = new PolicyVM();

            var polManager = new PolicyManager();
            var policy = polManager.Get(id);
            viewModel.Policy = policy;

            var catManager = new CategoryManager();
            viewModel.SetCategoryItems(catManager.List());

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(PolicyVM policyVM)
        {
            var catManager = new CategoryManager();
            policyVM.Policy.PolicyCategory = catManager.Get(policyVM.Policy.PolicyCategory.CategoryId);

            var polManager = new PolicyManager();
            polManager.Edit(policyVM.Policy);

            return RedirectToAction("Manage");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var polManager = new PolicyManager();
            var pol = polManager.Get(id);
            return View(pol);
        }

        [HttpPost]
        public ActionResult Delete(Policy policy)
        {
            var polManager = new PolicyManager();
            polManager.Delete(policy.PolicyId);
            return RedirectToAction("Manage");
        }
    }
}