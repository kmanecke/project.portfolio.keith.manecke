﻿$(document)
    .ready(function () {
        $('#addPositionForm')
            .validate({
                rules: {
                    PositionTitle: {
                        required: true,
                        minlength: 4
                    },
                    PostedSalary: {
                        required: true
                        
                    },
                    HiringManager: {
                        required: true
                    }

                },
                messages: {
                    PositionTitle: {
                        required: "Enter valid position title.",
                        minlength: $.validator
                            .format("The title cannot be fewere than {0} characters...")
                    },
                    PostedSalary: "You must include a salary",
                    HiringManager: "This field required"
                        
                }
            }
            )
    })