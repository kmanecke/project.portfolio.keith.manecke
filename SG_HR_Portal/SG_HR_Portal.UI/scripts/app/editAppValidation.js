﻿$(document)
    .ready(function() {
            $('#editAppForm')
                .validate({
                    rules: {
                        ApplicantName: {
                            required: true,
                            minlength: 3
                        },
                        ApplicantEmail: {
                            required: true,
                            email: true
                        }
                        
                    },
                    messages: {
                        ApplicantName: {
                            required: "Enter your name, por favor.",
                            minlength: $.validator
                                .format("I don't trust anyone whose name is less than {0} characters...")
                        },
                        ApplicantEmail: {
                            required: "Type in your email!!!",
                            email: "That's not the right way to type your email!"
                        }

                    }
                }
                )
        })