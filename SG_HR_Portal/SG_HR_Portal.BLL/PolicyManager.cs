﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Data.Factories;
using SG_HR_Portal.Data.Interfaces;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.BLL
{
    public class PolicyManager
    {

        private IPolicyRepository _repo;

        public PolicyManager()
        {
            _repo = PolicyRepositoryFactory.GetPolicyRepository();
        }

        public IEnumerable<Policy> List()
        {
            var result = _repo.List();
            return result;
        }

        public Policy Get(int id)
        {
            var result = _repo.Get(id);
            return result;
        }

        public void Add(Policy policy)
        {
            _repo.Add(policy);
        }

        public void Edit(Policy policy)
        {
            _repo.Edit(policy);
        }

        public void Delete(int id)
        {
            _repo.Delete(id);
        }

    }
}
