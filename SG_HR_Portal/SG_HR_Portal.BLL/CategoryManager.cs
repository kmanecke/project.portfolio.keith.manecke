﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Data.Factories;
using SG_HR_Portal.Data.Interfaces;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.BLL
{
    public class CategoryManager
    {
        private ICategoryRepository _repo;

        public CategoryManager()
        {
            _repo = CategoryRepositoryFactory.GetCategoryRepository();
        }

        public IEnumerable<Category> List()
        {
            var result = _repo.List();
            return result;
        }

        public Category Get(int id)
        {
            var result = _repo.Get(id);
            return result;
        }

        public void Add(Category category)
        {
            _repo.Add(category);
        }

        public void Edit(Category category)
        {
            _repo.Edit(category);
        }

        public void Delete(int id)
        {
            _repo.Delete(id);
        }
    }
}
