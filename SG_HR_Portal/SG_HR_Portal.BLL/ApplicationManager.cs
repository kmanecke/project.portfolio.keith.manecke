﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Data.Factories;
using SG_HR_Portal.Data.Interfaces;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.BLL
{
    public class ApplicationManager
    {
        private IApplicationRepository _repo;

        public ApplicationManager()
        {
            _repo = ApplicationRepositoryFactory.GetApplicationRepository();
        }

        public IEnumerable<Application> List()
        {
            var result = _repo.List();
            return result;
        }

        public Application Get(int id)
        {
            var result = _repo.Get(id);
            return result;
        }

        public void Add(Application application)
        {
            _repo.Add(application);
        }

        public void Edit(Application application)
        {
            _repo.Edit(application);
        }

        public void Delete(int id)
        {
            _repo.Delete(id);
        }
    }
}
