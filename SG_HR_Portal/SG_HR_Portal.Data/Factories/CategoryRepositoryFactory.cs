﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Data.Interfaces;
using SG_HR_Portal.Data.Repositories;

namespace SG_HR_Portal.Data.Factories
{
    public static class CategoryRepositoryFactory
    {
        public static ICategoryRepository GetCategoryRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Prod":
                    return new CategoryRepository();
                case "Test":
                    return new MockCategoryRepository();
                default:
                    throw new ArgumentException();
            }
        }
    }
}
