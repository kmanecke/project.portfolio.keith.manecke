﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Data.Interfaces;
using SG_HR_Portal.Data.Repositories;

namespace SG_HR_Portal.Data.Factories
{
    public static class PositionRepositoryFactory
    {
        public static IPositionRepository GetPositionRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Prod":
                    return new PositionRepository();
                case "Test":
                    return new MockPositionRepository();
                default:
                    throw new ArgumentException();
            }
        }
    }
}
