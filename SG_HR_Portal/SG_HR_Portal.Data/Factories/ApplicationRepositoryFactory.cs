﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Data.Interfaces;
using SG_HR_Portal.Data.Repositories;

namespace SG_HR_Portal.Data.Factories
{
    public static class ApplicationRepositoryFactory
    {
        public static IApplicationRepository GetApplicationRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Prod":
                    return new ApplicationRepository();
                case "Test":
                    return new MockApplicationRepository();
                default:
                    throw new ArgumentException();
            }
        }
    }
}
