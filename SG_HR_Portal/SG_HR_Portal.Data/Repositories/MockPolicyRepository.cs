﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Data.Interfaces;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Data.Repositories
{
    public class MockPolicyRepository : IPolicyRepository
    {

        private static List<Policy> policies;

        static MockPolicyRepository()
        {
            policies = new List<Policy>()
            {
                new Policy()
                {
                    PolicyId = 1,
                    PolicyTitle = "Mentoring Program",
                    PolicyCategory = new Category {CategoryId = 1, CategoryTitle = "Onboarding New Hires"},
                    PolicyContent = "All new hires will be assigned a mentor from the senior staff, who will meet with the mentee on a weekly basis " +
                                    "for the first three months of the mentee's employment."
                },
                new Policy()
                {
                    PolicyId = 2,
                    PolicyTitle = "Sick Leave",
                    PolicyCategory = new Category {CategoryId = 2, CategoryTitle = "Leave Time"},
                    PolicyContent = "All staff will accrue 1 sick day for each month worked. New hires will begin with a sick-leave balance of 3 days."
                },
                new Policy()
                {
                    PolicyId = 3,
                    PolicyTitle = "Vacation Leave",
                    PolicyCategory = new Category {CategoryId = 2, CategoryTitle = "Leave Time"},
                    PolicyContent = "Staff who have been employed for fewer than 5 years will accrue 1 vacation day for each month worked. Staff employed " +
                                    "for 5 years or more will accrue 1.5 vacation days for each month worked."
                }
            };
        }

        public IEnumerable<Policy> List()
        {
            return policies;
        }

        public Policy Get(int policyId)
        {
            return policies.FirstOrDefault(p => p.PolicyId == policyId);
        }

        public void Add(Policy policy)
        {
            var num = policies.Max(p => p.PolicyId) + 1;
            policy.PolicyId = num;
            policies.Add(policy);
        }

        public void Edit(Policy policy)
        {
            var policyToUpdate = policies.First(p => p.PolicyId == policy.PolicyId);
            policyToUpdate.PolicyTitle = policy.PolicyTitle;
            policyToUpdate.PolicyCategory = policy.PolicyCategory;
            policyToUpdate.PolicyContent = policy.PolicyContent;
        }

        public void Delete(int policyId)
        {
            var policyToDelete = policies.First(p => p.PolicyId == policyId);
            policies.Remove(policyToDelete);
        }
    }
}
