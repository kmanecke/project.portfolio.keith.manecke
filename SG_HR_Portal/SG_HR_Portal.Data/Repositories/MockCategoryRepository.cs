﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Data.Interfaces;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Data.Repositories
{
    public class MockCategoryRepository : ICategoryRepository
    {

        private static List<Category> categories;

        static MockCategoryRepository()
        {
            categories = new List<Category>()
            {
                new Category()
                {
                    CategoryId = 1,
                    CategoryTitle = "Onboarding New Hires",
                    CategoryDescription = "Policies related to new hires to the organization, and to the organization's efforts " +
                                          "to help facilitate the transition for new employees."
                },
                new Category()
                {
                    CategoryId = 2,
                    CategoryTitle = "Leave Time",
                    CategoryDescription = "Policies related to the accrual, usage, and maintenance of employee leave time."
                }
            };
        }

        public IEnumerable<Category> List()
        {
            return categories;
        }

        public Category Get(int categoryId)
        {
            return categories.FirstOrDefault((c => c.CategoryId == categoryId));
        }

        public void Add(Category category)
        {
            var num = categories.Max(c => c.CategoryId) + 1;
            category.CategoryId = num;
            categories.Add(category);
        }

        public void Edit(Category category)
        {
            var categoryToUpdate = categories.First(c => c.CategoryId == category.CategoryId);
            categoryToUpdate.CategoryTitle = category.CategoryTitle;
            categoryToUpdate.CategoryDescription = category.CategoryDescription;
        }

        public void Delete(int categoryId)
        {
            var categoryToDelete = categories.First(c => c.CategoryId == categoryId);
            categories.Remove(categoryToDelete);
        }
    }
}
