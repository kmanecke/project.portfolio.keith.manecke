﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Data.Interfaces;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Data.Repositories
{
    public class PolicyRepository : IPolicyRepository
    {
        public IEnumerable<Policy> List()
        {
            throw new NotImplementedException();
        }

        public Policy Get(int policyId)
        {
            throw new NotImplementedException();
        }

        public void Add(Policy policy)
        {
            throw new NotImplementedException();
        }

        public void Edit(Policy policy)
        {
            throw new NotImplementedException();
        }

        public void Delete(int policyId)
        {
            throw new NotImplementedException();
        }
    }
}
