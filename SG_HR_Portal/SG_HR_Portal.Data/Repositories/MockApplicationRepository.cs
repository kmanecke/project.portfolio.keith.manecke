﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Data.Interfaces;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Data.Repositories
{
    public class MockApplicationRepository : IApplicationRepository
    {

        private static List<Application> applications;

        static MockApplicationRepository()
        {
            applications = new List<Application>()
            {
                new Application()
                {
                    ApplicationId = 1,
                    ApplicantName = "Keith Manecke",
                    ApplicantEmail = "keith@email.com",
                    Position = new Position { PositionId = 1, PositionTitle = "Junior Developer"}
                },
                new Application()
                {
                    ApplicationId = 2,
                    ApplicantName = "Dave Balzer",
                    ApplicantEmail = "dave@email.com",
                    Position = new Position { PositionId = 2, PositionTitle = "Senior Developer"}
                },
                new Application()
                {
                    ApplicationId = 3,
                    ApplicantName = "Kristina Emick",
                    ApplicantEmail = "kristina@email.com",
                    Position = new Position { PositionId = 3, PositionTitle = "Communications Director"}
                }
            };
        }

        public IEnumerable<Application> List()
        {
            return applications;
        }

        public Application Get(int applicationId)
        {
            return applications.FirstOrDefault(a => a.ApplicationId == applicationId);
        }

        public void Add(Application application)
        {
            var num = applications.Max(a => a.ApplicationId) + 1;
            application.ApplicationId = num;
            applications.Add(application);
        }

        public void Edit(Application application)
        {
            var applicationToUpdate = applications.First(a => a.ApplicationId == application.ApplicationId);
            applicationToUpdate.ApplicantName = application.ApplicantName;
            applicationToUpdate.ApplicantEmail = application.ApplicantEmail;
            applicationToUpdate.Position = application.Position;
        }

        public void Delete(int applicationId)
        {
            var appToDelete = applications.First(a => a.ApplicationId == applicationId);
            applications.Remove(appToDelete);
        }
    }
}
