﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Data.Interfaces;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Data.Repositories
{
    public class MockPositionRepository : IPositionRepository
    {
        private static List<Position> positions;

        static MockPositionRepository()
        {
            positions = new List<Position>()
            {
                new Position()
                {
                    PositionId = 1,
            PositionTitle = "Junior Developer", 
            PostedSalary = 50000M,
            HiringManager = "Big Boss Lady"

                },
                new Position()
                {
                    PositionId = 2,
            PositionTitle = "Senior Developer",
            PostedSalary = 70000M,
            HiringManager = "Big Boss Man"

                },
                new Position()
                {
                    PositionId = 3,
            PositionTitle = "Communications Director",
            PostedSalary = 80000M,
            HiringManager = "CEO Person"

                },
                new Position()
                {
                    PositionId = 4,
            PositionTitle = "Custodian",
            PostedSalary = 40000M,
            HiringManager = "Big Boss Lady"

                }
            };
        }

        public IEnumerable<Position> List()
        {
            return positions;
        }

        public Position Get(int positionId)
        {
            return positions.FirstOrDefault(p => p.PositionId == positionId);
        }

        public void Add(Position position)
        {
            var num = positions.Max(p => p.PositionId) + 1;
            position.PositionId = num;
            positions.Add(position);
        }

        public void Edit(Position position)
        {
            var positionToUpdate = positions.First(p => p.PositionId == position.PositionId);
            positionToUpdate.PositionTitle = position.PositionTitle;
            positionToUpdate.PostedSalary = position.PostedSalary;
            positionToUpdate.HiringManager = position.HiringManager;
        }

        public void Delete(int positionId)
        {
            var positionToDelete = positions.First(p => p.PositionId == positionId);
            positions.Remove(positionToDelete);
        }
    }
}
