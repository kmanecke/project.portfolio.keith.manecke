﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Data.Interfaces;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Data.Repositories
{
    public class PositionRepository : IPositionRepository
    {
        public IEnumerable<Position> List()
        {
            throw new NotImplementedException();
        }

        public Position Get(int positionId)
        {
            throw new NotImplementedException();
        }

        public void Add(Position position)
        {
            throw new NotImplementedException();
        }

        public void Edit(Position position)
        {
            throw new NotImplementedException();
        }

        public void Delete(int positionId)
        {
            throw new NotImplementedException();
        }
    }
}
