﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Data.Interfaces;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Data.Repositories
{
    public class ApplicationRepository : IApplicationRepository
    {
        public IEnumerable<Application> List()
        {
            throw new NotImplementedException();
        }

        public Application Get(int applicationId)
        {
            throw new NotImplementedException();
        }

        public void Add(Application application)
        {
            throw new NotImplementedException();
        }

        public void Edit(Application application)
        {
            throw new NotImplementedException();
        }

        public void Delete(int applicationId)
        {
            throw new NotImplementedException();
        }
    }
}
