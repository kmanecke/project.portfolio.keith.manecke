﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Data.Interfaces
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> List();
        Category Get(int categoryId);
        void Add(Category category);
        void Edit(Category category);
        void Delete(int categoryId);
    }
}
