﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Data.Interfaces
{
    public interface IPolicyRepository
    {
        IEnumerable<Policy> List();
        Policy Get(int policyId);
        void Add(Policy policy);
        void Edit(Policy policy);
        void Delete(int policyId);
    }
}
