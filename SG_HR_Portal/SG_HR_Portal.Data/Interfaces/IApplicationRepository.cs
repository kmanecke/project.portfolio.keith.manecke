﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Data.Interfaces
{
    public interface IApplicationRepository
    {
        IEnumerable<Application> List();
        Application Get(int applicationId);
        void Add(Application application);
        void Edit(Application application);
        void Delete(int applicationId);    
    }
}
