﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Data.Interfaces
{
    public interface IPositionRepository
    {
        IEnumerable<Position> List();
        Position Get(int positionId);
        void Add(Position position);
        void Edit(Position position);
        void Delete(int positionId);
    }
}
