﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SG_HR_Portal.Models.DataModels;
using System.Web.Mvc;

namespace SG_HR_Portal.Models.ViewModels
{
    public class ApplicationVM
    {
        public Application Application { get; set; }
        public List<SelectListItem> PositionItems { get; set; }

        public ApplicationVM()
        {
            PositionItems = new List<SelectListItem>();
            Application = new Application();
        }

        public void SetPositionItems(IEnumerable<Position> positions)
        {
            foreach (var position in positions)
            {
                PositionItems.Add(new SelectListItem()
                {
                    Value = position.PositionId.ToString(),
                    Text = position.PositionTitle
                });
            }
        }
    }

}
