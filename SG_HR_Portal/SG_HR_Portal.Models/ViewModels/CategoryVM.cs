﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SG_HR_Portal.Models.DataModels;

namespace SG_HR_Portal.Models.ViewModels
{
    public class CategoryVM
    {
        public Category Category { get; set; }
        public List<SelectListItem> PolicyItems { get; set; }
        public List<int> AssignedPolicyIds { get; set; }

        public CategoryVM()
        {
            PolicyItems = new List<SelectListItem>();
            AssignedPolicyIds = new List<int>();
            Category = new Category();
        }

        public void SetPolicyItems(IEnumerable<Policy> policies)
        {
            foreach (var policy in policies)
            {
                PolicyItems.Add(new SelectListItem()
                {
                    Value = policy.PolicyId.ToString(),
                    Text = policy.PolicyTitle
                });
            }
        }
    }
}
