﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SG_HR_Portal.Models.DataModels
{
    public class Category
    {
        
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "A title is required.")]
        [DisplayName("Category")]
        public string CategoryTitle { get; set; }

        [Required(ErrorMessage = "A description of the category is required.")]
        [DisplayName("Description")]
        public string CategoryDescription { get; set; }
    }
}
