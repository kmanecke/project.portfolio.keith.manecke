﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SG_HR_Portal.Models.DataModels
{
    public class Policy
    {
        public int PolicyId { get; set; }

        [DisplayName("Policy")]
        public string PolicyTitle { get; set; }

        [DisplayName("Category")]
        public Category PolicyCategory { get; set; }

        [DisplayName("Content")]
        public string PolicyContent { get; set; }
    }
}
