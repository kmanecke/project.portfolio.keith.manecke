﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SG_HR_Portal.Models.DataModels
{
    public class Application
    {
        public int ApplicationId { get; set; }

        [DisplayName("Name")]
        public string ApplicantName { get; set; }

        [DisplayName("Email")]
        public string ApplicantEmail { get; set; }

        public Position Position { get; set; }
    }
}
