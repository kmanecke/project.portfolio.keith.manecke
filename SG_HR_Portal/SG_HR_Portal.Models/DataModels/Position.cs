﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SG_HR_Portal.Models.DataModels
{
    public class Position
    {
        public int PositionId { get; set; }

        [Required(ErrorMessage = "Position title is required.")]
        [DisplayName("Position")]
        public string PositionTitle { get; set; }

        [Required(ErrorMessage = "A salary value is required.")]
        [DisplayName("Salary")]
        public decimal PostedSalary { get; set; }

        [Required(ErrorMessage = "A hiring manager is required.")]
        [DisplayName("Hiring Manager")]
        public string HiringManager { get; set; }
    }
}
